/**
 * Promise based input prompts?  That's CLI input with style!
 */
'use strict';
const chalk = require('chalk');
const out = require('./cli-output-style');

module.exports = (prompt, defaults, typeoption) =>
{
	if (!typeoption)
	{
		
		if (!defaults)
			defaults = '';
		
		typeoption = typeof defaults;
	}

	let response_defs;

	if (Array.isArray(typeoption))
	{
		//maybe see if valid responses doesn't contain it?
		response_defs = typeoption;
		typeoption = 'array';
	}
	
	let dprompt;
	let valid_responses;
	let mute_output = false;

	switch (typeoption)
	{
		case 'boolean':
			if (defaults)
				dprompt = chalk.green('Y') + '/n';
			else
				dprompt = 'y/' + chalk.green('N') ;
			defaults = defaults ? 'y' : 'n';
			valid_responses = ['y','n','t','f','true','false'];
			break;

		case 'number':
			dprompt = 'number';
			if (defaults)
				dprompt += ', defaults to ' + defaults;
			break;

		case 'array':
			valid_responses = [];
			dprompt = response_defs.map(o =>
			{
				valid_responses.push(o[0].toLowerCase());

				let ret = `${o[1]} (${o[0]})`;
				if (o[0] === defaults)
				{
					ret = chalk.green(ret);
				};
				return ret;
			}).join(', ');
			break;

		case 'object':
			dprompt = '';
			break;

		case 'password':
			mute_output = true;
			dprompt = '';
			break;

		default: 
			if (defaults.toString)
				dprompt = 'defaults to ' + chalk.green(defaults.toString());
			else
				dprompt = '';
	};

	let stdin = process.openStdin();
	let res, rej;
	let retry_count = 0;

	let make_promise = () => new Promise((r,j) => 
	{
		res = r;
		rej = j;
		show();
	});

	let show = () => 
	{
		console.log(chalk.bold.cyan('--?>'), prompt);
		if (dprompt)
			console.log(chalk.bold.cyan('   >'), dprompt);

		process.stdout.write(chalk.bold.yellow('   > '));
	};

	let listener = data =>
	{
		let str = data.toString().trim();

		if (str.length === 0)
		{
			str = defaults;
			process.stdout.write(defaults.toString());
		}

		switch (typeoption)
		{
			case 'boolean':
				str = str.toLowerCase();
			
				if (valid_responses.indexOf(str) < 0)
				{
					if (++retry_count > 5)
						return rej(Error('invalid data entered.'));

					out.error(chalk.red('"' + str + ' is not a valid option."'));
					show();
					return;
				};
				break;

			case 'number':
				if (isNaN(str))
				{
					if (++retry_count > 5)
						return rej(Error('invalid data entered.'));

					out.error(chalk.red(' "' + str + '" is not a number.'));
					show();
					return;
				};
				break;

			case 'string':
			default:
				break;

		}

		stdin.removeListener('data', listener);

		switch (typeoption)
		{
			case 'boolean':
				return res(str);

			case 'number':
				return res(parseNumeric(str));

			default :
				return res(str);
		}
	}

	stdin.addListener('data', listener);

	return make_promise();
};
