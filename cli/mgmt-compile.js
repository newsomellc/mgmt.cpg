/** 
 * Compiles client-side code ahead of time.  I think the only way I'll support
 * running in production is if you do this. It's slow and risky to do this 
 * on-the-fly: that's only for personal use (i.e. on a local system not globally 
 * accessible) or as a convenience for development.
 */ 
'use strict';

//basically, just compile a client.js file that includes all the extra fixtures
//the user requested.

//though I don't know if it's possible to specify a list of includes to browserify 
//at compile-time-- it may be necessary for each fixture to compile its own client-side JS file.
//will be a nightmare if we need any dependencies.