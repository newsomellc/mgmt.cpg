/** 
 * Lists schemas, which correspond roughly to individual database tables you want
 * to edit.  
 */ 
'use strict';
const debug       = require('debug')('mgmt-cli-init');
const file_exists = require('fs').existsSync;
const pjoin       = require('path').join;
const NConf       = require('nconf').Provider;

const out = require('./cli-output-style');
const cin = require('./cli-input-style');

const program = new (require('commander')).Command('mgmt-init');

program
	.arguments('[directory]')
	.option('-e --env',     'configure for a certain environment');

//TODO: move these functions to their respective subcommands
//	.option('-f --fixture', 'generate skeleton of a custom fixture')
//	.option('-s --store',   'generate skeleton of a custom store')
//	.option('-i --skin',    'generate skeleton of a custom skin');

program.action(async (directory=process.cwd()) =>
{
	out.begin('Setting up mgmt');
	let out_config = new NConf();
	out_config.env();

	let filename = pjoin(directory, 'mgmt.json');

	if (file_exists(filename))
	{
		let options =
		[
			['a', 'adjust existing config'],
			['o', 'overwrite'],
			['q', 'quit'],
		];
		let answer = await cin('Configuration ' + filename + ' already exists. Proceed how?', 'a', options);

		switch (answer)
		{
			case 'a':
				out.info('loading existing config');
				out_config.file(filename);
				break;
			case 'o':
				out.info('overwriting existing config');
				break;
			case 'q':
			default:
				process.exit(0);
		}
	}
	//project directory (defaults to current)

	out_config.set('name', await cin('Human readable project name? (spaces, capitalization, punctuation are all OK)', out_config.get('name'), 'string'));
	out_config.set('listen', await cin('HTTP Listening port?', out_config.get('listen'), 'string'));

	
	if (!program.env && await cin('Do you want to create an environment config overlay?', false, 'boolean'))
	{
		let env = await cin('Which environment do you want to set up?', 'string', 'production');
		//call another instance of env setup.
	}

	let store_ok, schema_ok;

	if (await cin('Do you want to set up a your data store now?', true, 'boolean'))
	{
		console.error('todo: call mgmt store add here instead of this message');
		store_ok = true; //todo: only true if database connection is OK.
		if (await cin('Do you want to set up a schema for a database table in your data store?', true, 'boolean'))
		{
			schema_ok = true; //todo: only true if schema connection validates.
			console.error('todo: call mgmt schema add here instead of this message, also repeat if multiple (for convenience)');
		}
		else
			out.warn('Make sure to run', chalk.yellow('mgmt schema add'), 'to add schemas later.');
	}
	else
		out.warn('Make sure to run', chalk.yellow('mgmt store add'), 'to add a data store later.');

	if (await cin('Do you want to add a custom interface skin?', false, 'boolean'))
	{
		//from a package or in the current directory?
		//relative to project root, where?
		//generate skeleton with additional stuff
	}
	
	if (await cin('Do you want to set up custom fixtures now?', false, 'boolean'))
	{
		
		//relative to project root, where?
		//generate the skeleton of a fixture
	}
	
	if (store_ok && schema_ok && await cin('Do you want to run the admin interface now?', false, 'boolean'))
	{
		console.error('todo: call mgmt run here');
	}

});

program.parse(process.argv);
