#!/usr/bin/env node
/** 
 * Entrypoint for mgmt cli
 */
const chalk = require('chalk');
const program = new (require('commander')).Command('mgmt');

const splash = () =>
{
	console.log(chalk.cyan('MGMT.cpg'));
	console.log(chalk.blue('Admin interfaces on autopilot'));
	program.outputHelp();
}

process.on('unhandledRejection', (reason, p) => console.error('Unhandled Rejection at: Promise', p, 'reason:', reason));

program
	.command('run', 'starts management server').alias('r');

program
	.command('init', 'create a new mgmt system')



if (!process.argv.slice(2).length)
	splash();
else
	program
		.parse(process.argv);
