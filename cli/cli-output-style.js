/**
 * A quick definition of common output.  Makes it easy to maintain a consistent
 * appearance across every command.
 */
'use strict';
const chalk = require('chalk');

let rlog, clog, elog;

rlog = console.log;
clog = console.log;
elog = console.error;

exports.begin   = (...args) => clog(chalk.bold.green(     '->'   ), chalk.green(args.join(' ')));
exports.success = (...args) => clog(chalk.bold.green(     '>>>>' ), chalk.green(args.join(' ')));
exports.notice  = (...args) => clog(chalk.bold.cyan(      '  ->' ), args.join(' '));
exports.info    = (...args) => clog(chalk.blue(           '   *' ), args.join(' '));
exports.error   = (...args) => rlog(chalk.bold.yellow(    '  !>' ), args.join(' '));
exports.warn    = (...args) => elog(chalk.rgb(200,100,0)( ' !!>' ), args.join(' '));
exports.fatal   = (...args) => elog(chalk.bold.red(       '!!!>' ), chalk.bold.red(args.join(' ')));


//Set whether to send recoverable errors to stdout. Useful for example in 
//cron jobs, where you're silencing stdout but use stderr to call attention 
//to problems.
exports.recoverableToStderr = r2e => r2e ? rlog = console.error : rlog = console.log;
