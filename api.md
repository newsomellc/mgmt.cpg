# API 
This is just a quick description of the API, what the different parts are and how they work.  We don't do a lot of handholding, and code structure isn't enforced-- as long as the data structures are fed to us by our callbacks as we expect, it's fine. 

## How it works
Basically, the way I envision mgmt working is thus:

* As a CLI app:
you will have a mgmt.json in a directory.  Run mgmt from that directory to launch the management interface as defined there. 

* As an API dependency
    app.use('/mgmt', require('mgmt.cpg')(require('./mgmt.json')));
You'll want to make sure that /mgmt/js/client.js and /mgmt/css/client.css on your webserver resolve to pre-compiled JS and CSS files.

### Precompilation
In production, you'll want to pre-compile your scripts and CSS.


# Modules
## Index
Manages and initializes the fixtures, stores, and starting interface for the web app.  If mgmt is a factory, this is logistics/staging.

## App
The app is just the primary entry point for the program.  It manages express, websockets, and either generates or finds links to compiled CSS and JS.  If mgmt is a factory, this is the loading dock.

## Presence 
Manages a live websocket with an individual user.  There is no factory analogy for this, unless each delivery/shipping truck magically got its own loading dock where the driver had complete freedom to stash whatever he had wherever he wanted, and it would magically end up where it needed to go, and could ask for whatever he needed and get it loaded onto the truck instantly.

## Fixtures
A fixture is sort of analogous to a React.js component, but hopefully without all the boilerplate to make sure that barely-literate third world coders won't be able BS their way through making something compliant.

A fixture is created by supplying a constructor and/or a prototype to the default export of stores.js.

Stopping using the factory analogy here, because I'm imagining thousands of robot arms that go into each truck building things inside the container and skipping any kind of loading etc.

Fixtures are modules that are expected to provide methods for both client and server operation.  This will allow us to do things like serve images to the client for media library stores etc.

The name has nothing to do with fixtures as might be used in unit testing (though we do use those).  The name comes from thinking of them as light fixtures, faucets, or even outlets you can plug other things into in a room.  Rather than manufacturing an interface that can't be changed, think of building an interface as building a room where you add the specific things you need.

Fixtures need to be able to be compiled as a client-side script.  Don't rely on the state of anything other than the mgmt object which will be passed to it.  There's no direct DB access for example, only the `mgmt.stores.\<id\>('<table>')` knex-like interface (which passes queries over the websocket).

### Panel
A panel is a fixture that displays a panel of any kind on the screen.  Provide it with data, it will bind and everything will be cool.

### Widget 
A widget is a fixture that most closely imitates a react component.  Feed it an object, tell it which subkey it's following, and it'll bind proper-like.

### Listing
A listing displays more than one item, in a table, tree view etc.  MIGHT allow multi-edit at some point.

### Future
Other fixtures are possible-- spreadsheet?  Probably.  Rich text? Almost definitely.

### Todo:
Consider how to show other users' focus on the frontend, what they're currently working on, like Google-docs.

## Stores
A store is a (hopefully paper-thin as possible) wrapper around whatever data store you're using.  stores.Knex is provided by default-- you pass it a knex config object, and it gives you back a store that operates like a slightly limited version of the knex API. Only difference is we don't export  .then by default-- you must call .execute first.  Using .then to create a promise might save code, but it leads to confusing problems down the line.  A store is not duck-type compatible with a promise, IOW.  

I think of this as a regeneration of hypercard crossed with FileMaker.  Yeah, yeah, I know we're "past" that now, but there's a lot they got right.

### Knex
uses knex to provide access to an SQL backend.

### File
Gives lists of files with attributes etc.

### Hybrid?
Plan to create a hybrid store that will store files with a database backing.  Could use filesystem metadata (although there's no standardized thing for this). Might compose some underlying DB store with this, making it possible to track files without anything else.  Plan also to make it possible to edit

### Json
I plan to add a JSON datastore.  This system ISN'T just for storing data for a web server.  You might also use it to manage a media library, or as a replacement for iTunes.

### Config
A store that allows you to have a schema whose entire listing consists of one pane.  Useful for settings panels etc.  It just looks like a key->value store on the back end. 

TODO: make the JSON config file configurable from here?  Eeeeeeh.... maybe not?

## Client (directory)
Contains the basic css, js html and static files (or their precursors)

# CLI
All cli commands are contained in the cli subdirectory of this repo. It links a global executable: `mgmt`

## mgmt
main entry point. 
-c option specifies config file.  More than one -c option can be provided, with each subsequent file overlaying the last, overriding matching keys, as per standard nconf behavior.  Any config changes made always get written to the latest config file.

## mgmt run
runs the server, maybe even launches your browser for you (we're too nice)

## mgmt store
Manages stores, which store data.  

## mgmt fixture
Manages fixtures on your app.  Checks the code of the fixtures, warns of problems, maybe runs quick verifications.

## mgmt schema
Manages schemas, which are used to define individual tables.  Give it the table name, directory, what-have-you, and it will get the store to generate the schema in the current directory.  You can edit the schema to your liking.  You also have the option, for each field, or the entire table as a whole, to define which fixtures you want to use.

## mgmt setup
Automates and streamlines the process of setting up MGMT, asks you if you want a database etc.

# Roadmap
Everything not on the roadmap is considered necessary for a 1.0 release.  Things under here will come later.


## Migrations
I REALLY like Django's way of dealing with database migrations.  I'd like to, someday, implement that DRY approach in nodejs, though optionally.  The tutorials shouldn't posit it as the only way of doing things-- that's how you end up with bloat.  It should just be a thing you learn about later-- "Why didn't you tell me about this?" 

We can probably make it work by just generating JSON the describes the schema at an original known point, then write addenda that notates what changes after that-- what potential conversion routines may need to happen etc-- just like Django.  Then we can have some logic that interacts with knex to actually do the migrations.  Might have to store that ourselves.

It might not even be worth it, come to think of it-- Django migrations took a herculean effort to write, and they often lead to more complexity than they solve. It may be worthwhile to just lean on the user's own ability to learn to make things work. 

## CMS integration?
.cpg refers to Compages, a static site system we're working on.  The hope is to use this as an optional component, if you want to build pages from a database rather than from static files.  Also, having this set up will make things like locating files from a media library easily.

## User auth
Authentication is definitely something I'd like to add.  Auth should be pre-rolled, all server-side.  Permissions should be provided as a set of keys with simple yes/no etc.  It probably needs to be woven into everything, with permissions set even down to the per-field level on schemae (i.e. a permissions structure, with a list of roles that can see, change etc each field). Each fixture needs access to the current user context.  Presence is already pretty much per-user, so in the program hierarchy that's probably where it starts, and then propagates to everything under it.  Permissions should be obvious from the interface level (i.e. things will be grayed out, or fields made invisible), but ENFORCED at the database level-- i.e. if you trick the interface into showing you a column for a value you're not supposed to see, it'll just be blank when you query it.  Or you get an error.

I can probably have a dummy auth system built-in, that just answers "yes" to every "can you do X" question, which you replace if you want actual security.

