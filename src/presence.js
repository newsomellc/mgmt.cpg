/**
 * Represents a single presence on the authoring backend.
 */
'use strict';

const debug  = require('debug')('mgmt:presence');
const pjoin  = require('path').join;
const Moment = require('moment-timezone');
const chalk  = require('chalk');

let user = 0;

module.exports = (socket, mgmt) =>
{
	socket.broadcast.emit('user-connect', ++user);
};
