/** 
 * Exports something you can include in an express app.  Also need to figure
 * out how to hook up websocket in API mode.
 */ 
'use strict';

const pjoin  = require('path').join;
const Moment = require('moment-timezone');
const debug  = require('debug')('mgmt:app');
const chalk  = require('chalk');

module.exports = (config) =>
{	
	let app = require('express')();
	app.disable('x-powered-by');

	app.set('view engine', 'pug');
	app.set('views', pjoin(__dirname, 'client'));

	app.use(require('morgan')('dev'));

	app.use(require('node-sass-middleware')(
	{
		src            : pjoin(__dirname, 'client'),
		indentedSyntax : true,
		sourceMap      : false,
		response       : true,
		importer       : require('sass-module-importer')(),
	}));

	app.use(require('browserify-middleware')(pjoin(__dirname, 'client'),
	{
		transform:
		[
			['babelify', {presets: ['es2015',]} ],
			['pugify', {pretty : false } ],
		],
	}));

	app.use(require('express').static(pjoin(__dirname, 'client'), {redirect: false}));

	app.get('/', (req, res, next) =>
	{
		res.render('client', {});
	});

	app.use((req, res, next) =>
	{
		let error = Error('Not found.');
		error.code = 404;
		next(error);
	});

	/**
	 * Error handler.
	 */
	app.use((err, req, res, next) =>
	{
		console.error(chalk.bold.red('ERROR'));
		console.error(err);

		res.send('Error!');
	});

	let server = app.listen(config.listen);

	server.on('listening', () =>
	{
		debug('opening author page');
		if (config.browse)
			require('opn')('http://localhost:3001');
	});

	let io = require('socket.io')(server);

	io.on('connection', socket =>
	{
		console.log(chalk.green('got connection.'));
		require('./presence')(socket, null);
	});

	return app;
}
