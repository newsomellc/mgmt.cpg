/** 
 * Gets the configuration for crud system
 */ 
'use strict';
const debug  = require('debug')('mgmt:config');
const NConf  = require('nconf').Provider;
const exists = require('fs').existsSync;

module.exports = (params) =>
{
	config = NConf();

	//if params is a string, it references a config file.
	if (typeof config === 'string')
	{ 
		//TODO: check if file exists
		//TODO: allow environment overlay on explicit file
		debug('loading config file from', config);
		config.file(config);
	}
	else if (params)
	{
		//only other option is if config is an object, so it's been passed manually.
		//foreach key set in the conf object
		//do not load from a file at all.
	}
	//if there are no params, we proceed as normal.







	return config;
};



//Reads from parameter passed in from cli or api.

//reads from JSON.env file in CWD
//reads from JSON file in CWD.

//reads from JSON.env file in user home directory
//reads from JSON file in user home directory

//Probably we can stop there. Systemwide config is probably not needed.

