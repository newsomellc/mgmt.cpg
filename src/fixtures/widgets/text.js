'use strict';
//all widgets, as defined by users, are basically mixins to the main widget "class".
//ALL methods here are async.
//this.id       = the ID for this widget
//this.dom      = jquery context
//this.throw    = a function to pass errors to
//this.store    = a reference to the store that you can query against
//this.ready    = call when an async task is finished
//this.disabled = whether this input is disabled
//this.help     = short line of help text

//return HTML to display the given value in an edit field.
exports.field = function (pk, value)
{
	return `<input type="text" value="${value}" id="id" data-pk="${pk}" class="wdgt wdgt-text ${this.classes.join(' ')}">`;
};

//Render a table cell with the given value.
exports.cell = function (value)
{
	return value;
};

//given the value from $(id).val(), turn it into something JSON-representable.
exports.pack = function (rawvalue)
{
	return rawval;
};

//updates all outstanding references
exports.update = function (rawvalue)
{

};

//determines which css classes you need to show cells in a table
exports.cssCell = function (pk) 
{

};

//same, but on edit panels
exports.cssField = function (pk) 
{

};
