/** 
 * Sets up the crud system.
 */ 
'use strict';
const debug  = require('debug')('mgmt:index');


module.exports = ({options, schemas, store, widgets, panels}) =>
{
	//init the stores
	//init the fixtures (server-side-- client side gets prebuilt, or built on-the-fly by the app when served)
	//init the app
};

//all options below trigger a client reload event.

/**
 * Add get or update an option.
 */
const OPTION = function (key, value)
{

};

/**
 * Add, get or update a schema.
 */
const SCHEMA = function (id, params)
{

};

/**
 * Add, get or update a store. (database, REST, however we get info)
 */
const STORE = function (id, params)
{

};

/**
 * Add, get or update a listing panel
 */
const FIXTURE = function (id, params)
{
	
};

module.exports.stores   = require('./src/stores');
module.exports.fixtures = require('./src/fixtures');
