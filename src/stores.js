/** 
 * Sets up the interface to the data store.  Exists so that we can use any 
 * data store-- database, files, etc interchangeably.
 *
 * All stores should support a subset of knex functionality.  A store should be
 * fed a single parameter (analogous to a table name) that initializes which 
 * "table" it deals with.  This could be a folder path, for example, with file
 * stores.  This returns a knex-like object that offers at least where, whereIn
 * etc.  I'm not 100% sure I should force people to write chainable methods, 
 * but I'm not sure I shouldn't either.
 * 
 * This is an example of a metaclass, a class that's used to instantiate classes,
 * (NOT AT ALL THE SAME THING AS INHERITANCE) and how difficult it is to do things
 * like this (or even imagine you might want to) in bog standard OO is why I
 * revile it.  You have between your abstraction and the bare metal many 
 * possibilities that get ignored because your abstraction is so far from 
 * how computers work that it makes them prohibitively complicated.
 */
'use strict';
const debug = require('debug')('mgmt:stores');
const pjoin = require('path').join;
const chalk = require('chalk');

module.exports =
	(constructor, prototype) =>
	{
		//I know all the warnings about this, but my hope is that only doing this very early in overall program setup
		//doesn't hurt engine optimization too much.  The only reason I do this here is to allow us to use instanceof
		//elsewhere, but I can sacrifice that if this is a problem, and just assign from the base if nonexistent.
		Object.setPrototypeOf(prototype, BASE_PROTOTYPE);
		return (params) =>
			constructor.call(Object.create(prototype), params);
	};

const BASE_PROTOTYPE = 
{
	then : () => 
	{
		throw Error('Stores are not directly thennable-- call .execute method to get a promise.');
	},
	execute : () =>
	{
		throw Error('.execute must be implemented.');
	},
};


module.exports.Knex = (config) =>
{
	debug(`init knex store: ${config.connection}`);

	try
	{
		let knex = require('knex')(config);
	}
	catch (err)
	{
		console.error('Knex doesn\'t appear to be installed.  Run', chalk.yellow('npm install --save knex'), 'to install it.');
	}

	const KNEX_CONSTRUCT =  function (table_name)
	{
		this.__knex = knex(table_name);
	};

	const KNEX_PROTOTYPE =
	{
		where : function (...params)
		{
			this.__knex.where(...params);
			return this;
		},

		whereIn : function (...params)
		{
			this.__knex.whereIn(...params);
			return this;
		},

		orderBy : function (field, direction='asc')
		{
			this.__knex.orderBy(field, direction);
			return this;
		},

		patch : function (...params) 
		{
			this.__knex.patch(...params);
			return this;
		},

		insert : function (...params)
		{
			this.__knex.insert(...params);
			return this;
		},

		execute : function ()
		{
			this.then = (...params) => this.__knex.then(...params);
			return this;
		},
	};

	return module.exports(KNEX_CONSTRUCT, KNEX_PROTOTYPE);
};

/* just going to comment this out for now.  It's a good plan, but I'll get something working first.
module.exports.Directory = (config) =>
{
	const extensions  = config.extensions || ['*']; //which extensions to include.  Empty array means everything.
	const directories = config.directories;//whether to include directories as entities.  Independent of recursive below.
	const recursive   = config.recursive; //whether we include the files in subdirectories (as opposed to just listing the directories themselves)


	const DIRECTORY_CONSTRUCT = function (directory, parent)
	{
		this.wheres   = [];
		this.orderby  = [];
		this.whereins = [];
		
		this.directory = directory;
	
		if (parent)
		{
			this.file_prototype = Object.create(file_prototype);
			this.file_prototype.directory = directory;
		}
		else
		{
			this.file_prototype
		}
	};

	const DIRECTORY_PROTOTYPE = 
	{
		where : function (...params)
		{
			this.wheres.push(params);
		},
		whereIn : function (...params)
		{
			this.whereins.push(params);
		},
		orderBy : function (...params)
		{
			this.orderbys.push(params);
		},
		//get a store representing a subdirectory
		subdirectory : dirname => DIRECTORY_CONSTRUCT(pjoin(this.directory, dirname), this),
		execute : function ()
		{
			return new Promise((res, rej) =>
			{
				//write query here.
			});
		},
	};

	return module.exports(DIRECTORY_CONSTRUCT, DIRECTORY_PROTOTYPE);
};
*/